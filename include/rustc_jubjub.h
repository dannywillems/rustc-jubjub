#ifndef RUSTC_JUBJUB_INCLUDE_H
#define RUSTC_JUBJUB_INCLUDE_H
#include <stdbool.h>

extern void rustc_jubjub_zero(unsigned char *buffer);

extern void rustc_jubjub_one(unsigned char *buffer);

extern bool rustc_jubjub_is_zero(const unsigned char *g);

extern bool rustc_jubjub_check_bytes(const unsigned char *element);

extern void rustc_jubjub_add(unsigned char *buffer,
                             const unsigned char* g1,
                             const unsigned char *g2);

extern void rustc_jubjub_negate(unsigned char *buffer,
                                const unsigned char *g);

extern void rustc_jubjub_double(unsigned char *buffer, const unsigned char *g);

extern bool rustc_jubjub_eq(const unsigned char *g1,
                            const unsigned char *g2);


extern void rustc_jubjub_mul(unsigned char *buffer,
                             const unsigned char *g,
                             const unsigned char *a);

extern void rustc_jubjub_get_u_coordinate(unsigned char *buffer, const unsigned char *g);

extern void rustc_jubjub_get_v_coordinate(unsigned char *buffer,
                                          const unsigned char *g);

extern void rustc_jubjub_from_coordinates(unsigned char *buffer,
                                          const unsigned char *u,
                                          const unsigned char *v);

extern bool rustc_jubjub_is_torsion_free(const unsigned char *buffer);

extern void rustc_jubjub_multiply_by_cofactor(unsigned char *buffer,
                                              const unsigned char *g);

#endif
